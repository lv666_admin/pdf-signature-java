package top.cnspt.pdf.utils;

import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Pattern;

public class Empty {
    /**
     * 空值检查
     * @param pInput 要检查的字符串
     * @return boolean 返回检查结果,但传入的字符串为空的场合,返回真
     */
    public static boolean isNull(Object pInput) {
        // 判断参数是否为空或者''
        if (pInput == null || "".equals(pInput)) {
            return true;
        } else if ("java.lang.String".equals(pInput.getClass().getName())) {
            // 判断传入的参数的String类型  替换各种空格
            String tmpInput = Pattern.compile("//r|//n|//u3000").matcher((String) pInput).replaceAll("");
            // 匹配空
            return Pattern.compile("^(//s)*$").matcher(tmpInput).matches();
        } else {
            // 方法类
            Method method = null;
            String newInput = "";
            try {
                // 访问传入参数的size方法
                method = pInput.getClass().getMethod("size");
                // 判断size大小

                // 转换为String类型
                newInput = String.valueOf(method.invoke(pInput));
                // size为0的场合
                if (Integer.parseInt(newInput) == 0) {
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e) {
                // 访问失败
                try {
                    // 访问传入参数的getItemCount方法
                    method = pInput.getClass().getMethod("getItemCount");
                    // 判断size大小
                    // 转换为String类型
                    newInput = String.valueOf(method.invoke(pInput));

                    // getItemCount为0的场合
                    if (Integer.parseInt(newInput) == 0) {
                        return true;
                    } else {
                        return false;
                    }
                } catch (Exception ex) {
                    // 访问失败
                    try {
                        // 判断传入参数的长度

                        // 长度为0的场合
                        if (Array.getLength(pInput) == 0) {
                            return true;
                        } else {
                            return false;
                        }
                    } catch (Exception exx) {
                        // 访问失败
                        try {
                            // 访问传入参数的hasNext方法
                            method = Iterator.class.getMethod("hasNext");
                            // 转换String类型
                            newInput = String.valueOf(method.invoke(pInput));

                            // 转换hasNext的值
                            if (!Boolean.valueOf(newInput)) {
                                return true;
                            } else {
                                return false;
                            }

                        } catch (Exception exxx) {
                            // 以上场合不满足
                            return false;
                        }
                    }
                }
            }
        }
    }
}
