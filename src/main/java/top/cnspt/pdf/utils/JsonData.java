package top.cnspt.pdf.utils;

public class JsonData {
    private int code;

    private Object data;

    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public JsonData(){}

    public JsonData(int code, Object data){
        this.code = code;
        this.data = data;

    }

    public JsonData(int code, Object data, String msg){
        this.code = code;
        this.data =data;
        this.msg = msg;

    }

    public static JsonData success(Object data){
        return new JsonData(200,data);
    }

    public static JsonData success(String msg){
        return new JsonData(200, "", msg);
    }

    public static JsonData Error(String msg){
        return new JsonData(300,"",msg);
    }

    public static JsonData Error(String msg,int code){
        return new JsonData(code,"",msg);
    }
}
